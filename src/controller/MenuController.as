package controller
{
	import flash.display.NativeMenu;
	import flash.display.NativeMenuItem;
	import flash.events.Event;
	
	import mx.managers.PopUpManager;
	
	import popups.AboutPopup;
	import popups.ClearHistoryPopup;
	import popups.MasterPasswordPopup;
	import popups.administrators.AdminPopup;
	import popups.device.DeviceReportPopup;
	import popups.device.DevicesPopup;
	import popups.users.UsersPopup;

	public class MenuController
	{
		private var _application:MobileDeviceManagement;
		
		public function MenuController(application:MobileDeviceManagement)
		{
			_application = application;
			
			initMenu();
		}
		
		private function initMenu():void{
			var fileMenu:NativeMenuItem;
			var helpMenu:NativeMenuItem;
			
			_application.stage.nativeWindow.menu = new NativeMenu();
			
			fileMenu = _application.stage.nativeWindow.menu.addItem(new NativeMenuItem("Config"));
			fileMenu.submenu = createFileMenu();
			
			helpMenu = _application.stage.nativeWindow.menu.addItem(new NativeMenuItem("Help"));
			helpMenu.submenu = createHelpMenu();
		}
		
		private function createFileMenu():NativeMenu{
			var configMenu:NativeMenu = new NativeMenu();
			
			var manageDeviceMenuItem:NativeMenuItem = configMenu.addItem(new NativeMenuItem("Manage Devices ..."));
			manageDeviceMenuItem.addEventListener(Event.SELECT, selectCommand);
			
			var deviceReportMenuItem:NativeMenuItem = configMenu.addItem(new NativeMenuItem("Devices Report ..."));
			deviceReportMenuItem.addEventListener(Event.SELECT, selectCommand);
			
			configMenu.addItem(new NativeMenuItem("", true));
			
			var manageUsersMenuItem:NativeMenuItem = configMenu.addItem(new NativeMenuItem("Manage Users ..."));
			manageUsersMenuItem.addEventListener(Event.SELECT, selectCommand);
			
			var manageAdminsMenuItem:NativeMenuItem = configMenu.addItem(new NativeMenuItem("Manage Administrators ..."));
			manageAdminsMenuItem.addEventListener(Event.SELECT, selectCommand);
			
			var masterPasswordMenuItem:NativeMenuItem = configMenu.addItem(new NativeMenuItem("Change Master Password ..."));
			masterPasswordMenuItem.addEventListener(Event.SELECT, selectCommand);
			
			var clearHistoryMenuItem:NativeMenuItem = configMenu.addItem(new NativeMenuItem("Clear Device History ..."));
			clearHistoryMenuItem.addEventListener(Event.SELECT, selectCommand);
			
			configMenu.addItem(new NativeMenuItem("", true));
			
			var exitMenuItem:NativeMenuItem = configMenu.addItem(new NativeMenuItem("Exit"));
			exitMenuItem.addEventListener(Event.SELECT, selectCommand);
			
			return configMenu;
		}
		
		private function createHelpMenu():NativeMenu{
			var helpMenu:NativeMenu = new NativeMenu();
			
			var aboutMenuItem:NativeMenuItem = helpMenu.addItem(new NativeMenuItem("About Mobile Device Management ..."));
			aboutMenuItem.addEventListener(Event.SELECT, selectCommand);
			
			return helpMenu;
		}
		
		//Handlers
		protected function selectCommand(event:Event):void{
			if(NativeMenuItem(event.target).label == "Manage Devices ..."){
				var devicesPopup:DevicesPopup = new DevicesPopup();
				PopUpManager.addPopUp(devicesPopup, _application);
				PopUpManager.centerPopUp(devicesPopup);
				
				devicesPopup.addEventListener(Event.REMOVED_FROM_STAGE, onDevicePopupClose, false, 0, true);
			}else if(NativeMenuItem(event.target).label == "Devices Report ..."){
				var deviceReportPopup:DeviceReportPopup = new DeviceReportPopup();
				deviceReportPopup.deviceIndex = _application.deviceComboBox.selectedIndex;
				
				PopUpManager.addPopUp(deviceReportPopup, _application);
				PopUpManager.centerPopUp(deviceReportPopup);
			}else if(NativeMenuItem(event.target).label == "Manage Users ..."){
				var usersPopup:UsersPopup = new UsersPopup();
				PopUpManager.addPopUp(usersPopup, _application);
				PopUpManager.centerPopUp(usersPopup);
				
				usersPopup.addEventListener(Event.REMOVED_FROM_STAGE, onUserPopupClose, false, 0, true);
			}else if(NativeMenuItem(event.target).label == "Manage Administrators ..."){
				var adminPopup:AdminPopup = new AdminPopup();
				PopUpManager.addPopUp(adminPopup, _application);
				PopUpManager.centerPopUp(adminPopup);
			}else if(NativeMenuItem(event.target).label == "Change Master Password ..."){
				var masterPasswordPopup:MasterPasswordPopup = new MasterPasswordPopup();
				PopUpManager.addPopUp(masterPasswordPopup, _application);
				PopUpManager.centerPopUp(masterPasswordPopup);
			}else if(NativeMenuItem(event.target).label == "Clear Device History ..."){
				var clearHistoryPopup:ClearHistoryPopup = new ClearHistoryPopup();
				PopUpManager.addPopUp(clearHistoryPopup, _application);
				PopUpManager.centerPopUp(clearHistoryPopup);
				clearHistoryPopup.addEventListener(Event.REMOVED_FROM_STAGE, onClearHistoryPopupClose, false, 0, true);
			}else if(NativeMenuItem(event.target).label == "Exit"){
				_application.exit();
			}else if(NativeMenuItem(event.target).label == "About Mobile Device Management ..."){
				var aboutPopup:AboutPopup = PopUpManager.createPopUp(_application, AboutPopup, true) as AboutPopup;
				PopUpManager.centerPopUp(aboutPopup);
			}
		}
		
		protected function onDevicePopupClose(event:Event):void
		{
			event.target.removeEventListener(Event.REMOVED_FROM_STAGE, onDevicePopupClose);
			_application.appController.loadDevices();
		}
		
		protected function onUserPopupClose(event:Event):void
		{
			event.target.removeEventListener(Event.REMOVED_FROM_STAGE, onDevicePopupClose);
			_application.appController.loadUsers();
		}
		
		protected function onClearHistoryPopupClose(event:Event):void
		{
			event.target.removeEventListener(Event.REMOVED_FROM_STAGE, onClearHistoryPopupClose);
			_application.appController.loadLoans();
		}

	}
}