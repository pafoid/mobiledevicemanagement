package controller
{
	import com.pafoid.sharedObject.SharedObjectManager;
	
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	
	import mx.collections.ArrayList;
	import mx.controls.Alert;
	import mx.managers.PopUpManager;
	
	import spark.events.GridSelectionEvent;
	
	import data.DeviceVO;
	import data.LoanVO;
	import data.SharedObjectKeys;
	import data.UserVO;
	
	import popups.device.DeviceAvailabilityPopup;
	import popups.device.DeviceReportPopup;

	public class ApplicationController
	{
		private var _application:MobileDeviceManagement;
		private var _deviceList:ArrayList;
		private var _userList:ArrayList;
		private var _adminList:ArrayList;
		
		[Bindable]
		private var _loanList:ArrayList;
		
		public function ApplicationController(application:MobileDeviceManagement)
		{
			_application = application;
			
			//resetLoans();
			
			initMasterPassword();
			loadDevices();
			loadUsers();
			loadAdmins();
			loadLoans();
			
			initUI();
		}
		
		//Debug only
		public function resetLoans():void{
			SharedObjectManager.instance.save(SharedObjectKeys.LOANS, []);
		}
		
		private function initUI():void{
			_application.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown, false, 0, true);
		}
		
		//Models
		private function initMasterPassword():void{
			var masterPassword:String = SharedObjectManager.instance.getStringValue(SharedObjectKeys.MASTER_PASSWORD);
			
			if(masterPassword == null){
				masterPassword = "mdm";
				SharedObjectManager.instance.save(SharedObjectKeys.MASTER_PASSWORD, masterPassword);
			}
		}
		
		public function loadDevices():void{
			var devicesArray:Array = SharedObjectManager.instance.getValue(SharedObjectKeys.DEVICES);
			_deviceList = new ArrayList(devicesArray);
			
			_application.deviceComboBox.dataProvider = _deviceList;
			_application.deviceComboBox.selectedIndex = 0;
		}
		
		public function loadUsers():void{
			var usersArray:Array = SharedObjectManager.instance.getValue(SharedObjectKeys.USERS);
			usersArray ||= [];
			
			if(usersArray.length == 0){
				usersArray.push(new UserVO("Device Drawer", "return"));
				SharedObjectManager.instance.save(SharedObjectKeys.USERS, usersArray);
			}
			
			_userList = new ArrayList(usersArray.sortOn("userName"));
			
			_application.userComboBox.dataProvider = _userList;
			_application.userComboBox.selectedIndex = 0;
		}
		
		public function loadAdmins():void{
			var adminsArray:Array = SharedObjectManager.instance.getValue(SharedObjectKeys.ADMINS);
			_adminList = new ArrayList(adminsArray.sortOn("userName"));
			
			_application.adminComboBox.dataProvider = _adminList;
			_application.adminComboBox.selectedIndex = 0;
		}
		
		public function loadLoans():void{
			var loanArray:Array = SharedObjectManager.instance.getValue(SharedObjectKeys.LOANS);
			loanArray ||= [];
			
			loanArray.reverse();			
			//var tempArray:Array = loanArray.slice(0, 5);
			
			_loanList = new ArrayList(loanArray);
			_application.loansGrid.invalidateDisplayList();
			_application.loansGrid.dataProvider = _loanList;
			_application.loansGrid.invalidateDisplayList();
		}
		
		private function getDeviceIndexByName(deviceName:String):int{
			for each (var deviceVO:Object in _deviceList.source) 
			{
				if(deviceVO.deviceName == deviceName)
					return _deviceList.source.indexOf(deviceVO);
			}
			
			return -1;
		}
		
		//Handlers
		public function onSelectLoan(event:GridSelectionEvent):void{
			_application.returnDeviceButton.enabled = (_application.loansGrid.selectedItem.userName != "Device Drawer");
		}
		
		private function onKeyDown(event:KeyboardEvent):void{
			if(event.keyCode == Keyboard.ENTER)
				onClickSave();
		}
		
		public function onClickHistory(event:MouseEvent):void{
			trace("history");
			var deviceReportPopup:DeviceReportPopup = new DeviceReportPopup();
			deviceReportPopup.deviceIndex = _application.deviceComboBox.selectedIndex;
			
			PopUpManager.addPopUp(deviceReportPopup, _application);
			PopUpManager.centerPopUp(deviceReportPopup);
		}
		
		public function onClickAvailability(event:MouseEvent):void{
			trace("availability");
			
			var deviceAvailabilityPopup:DeviceAvailabilityPopup = new DeviceAvailabilityPopup();
			deviceAvailabilityPopup.deviceIndex = _application.deviceComboBox.selectedIndex;
			
			PopUpManager.addPopUp(deviceAvailabilityPopup, _application);
			PopUpManager.centerPopUp(deviceAvailabilityPopup);
		}
		
		public function onClickReturn(event:MouseEvent):void{
			trace("return");	
			
			_application.deviceComboBox.selectedIndex = getDeviceIndexByName(_application.loansGrid.selectedItem.deviceName);
			_application.userComboBox.selectedIndex = 0;
			_application.userPwdInput.text = "return";
				
			onClickSave();
			
			_application.returnDeviceButton.enabled = false;
		}
		
		public function onClickSave(event:MouseEvent = null):void{
			trace("save");
			
			if(validateLoan()){
				var loanArray:Array = SharedObjectManager.instance.getValue(SharedObjectKeys.LOANS);
				loanArray ||= [];
				
				var deviceName:String = _application.deviceComboBox.selectedItem.deviceName;
				var userName:String = _application.userComboBox.selectedItem.userName;
				var adminName:String = _application.adminComboBox.selectedItem.userName;
				var dateString:String = getCurrentDate();
				
				var loanVO:LoanVO = new LoanVO(deviceName, userName, adminName, dateString);
				loanArray = loanArray.reverse();
				loanArray.push(loanVO);
				
				SharedObjectManager.instance.save(SharedObjectKeys.LOANS, loanArray);
				loadLoans();
				Alert.show("Loan Saved !", "Success");
				
				_application.userPwdInput.text = "";
				_application.adminPwdInput.text = "";
			}
		}
		
		private function validateLoan():Boolean{
			var result:Boolean = true;
			
			//Device
			if(_application.deviceComboBox.selectedIndex < 0){
				result = false;
				Alert.show("You must select a device to loan.", "Error");
			}
			
			var deviceOwner:String = isDeviceLoaned();
			if(deviceOwner != "Device Drawer" && deviceOwner != null && _application.userComboBox.textInput.text != "Device Drawer"){
				result = false;
				Alert.show("This device was already loaned by "+deviceOwner+", you must return it to the device drawer first.", "Error");
			}
			
			//User
			if(_application.userComboBox.selectedIndex < 0){
				result = false;
				Alert.show("You must select a user to loan.", "Error");
			}else{
				//Password
				var userArray:Array = SharedObjectManager.instance.getValue(SharedObjectKeys.USERS);
				var userPassword:String = userArray[_application.userComboBox.selectedIndex].password;
				if(_application.userPwdInput.text == ""){
					Alert.show("You must enter password for user "+_application.userComboBox.selectedItem.userName+".", "Error");
					return result = false;
				}
				
				if(_application.userPwdInput.text != userPassword){
					Alert.show("Wrong password for user "+_application.userComboBox.selectedItem.userName+".", "Error");
					return result = false;
				}
			}
			
			//Admin
			if(_application.adminComboBox.selectedIndex < 0){
				result = false;
				Alert.show("You must select an administrator to loan.", "Error");
			}else{
				//Password
				var adminArray:Array = SharedObjectManager.instance.getValue(SharedObjectKeys.ADMINS);
				var adminPassword:String = adminArray[_application.adminComboBox.selectedIndex].password;
				if(_application.adminPwdInput.text == ""){
					Alert.show("You must enter password for administrator "+_application.adminComboBox.selectedItem.userName+".", "Error");
					return result = false;
				}
				
				if(_application.adminPwdInput.text != adminPassword){
					Alert.show("Wrong password for admin "+_application.adminComboBox.selectedItem.userName+".", "Error");
					return result = false;
				}
			}
			
			return result;
		}
		
		private function isDeviceLoaned():String{
			var loanArray:Array = SharedObjectManager.instance.getValue(SharedObjectKeys.LOANS);
			var selectedDeviceName:String = _application.deviceComboBox.selectedItem.deviceName;
			var available:Boolean = true;
			var owner:String;
			
			for each (var loan:Object in loanArray) 
			{
				if(loan.deviceName == selectedDeviceName){						
					available = (loan.userName != "Device Drawer");
					owner = loan.userName;
					break;
				}
			}	
			
			return owner;
		}
		
		private function getCurrentDate():String{
			var result:String;
			var date:Date =  new Date();
			
			var minutes:String = (date.minutes.toString().length>1) ? date.minutes.toString() : "0"+date.minutes.toString();
			var seconds:String = (date.seconds.toString().length>1) ? date.seconds.toString() : "0"+date.seconds.toString();
			result = date.date.toString() + "/" + date.month.toString() + "/" + date.fullYear.toString() + " " + date.hours.toString() + ":" + minutes+":"+seconds; 
			
			return result;				
		}
	}
}