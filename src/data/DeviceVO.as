package data
{
	public class DeviceVO
	{
		public static const ANDROID:String = "Android";
		public static const IOS:String = "iOS";
		public static const NOOK:String = "Nook";
		public static const KINDLE:String = "Kindle";
		public static const SURFACE:String = "Surface";
		
		public static const STATUS_AVAILABLE:String = "available";
		public static const STATUS_LENDED:String = "lended";
		
		public var deviceName:String;
		public var deviceType:String;
		public var deviceStatus:String;
		
		public function DeviceVO(name:String, type:String, status:String = "available")
		{
			deviceName = name;
			deviceType = type;
			deviceStatus = status;
		}
	}
}