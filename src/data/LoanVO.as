package data
{
	public class LoanVO
	{
		public var deviceName:String;
		public var userName:String;
		public var adminName:String;
		public var date:String;
		
		public function LoanVO(device:String, user:String, admin:String, date:String)
		{
			deviceName = device;
			userName = user;
			adminName = admin;
			this.date = date;
		}
	}
}