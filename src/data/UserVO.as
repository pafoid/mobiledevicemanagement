package data
{
	public class UserVO
	{
		public var userName:String;
		public var password:String;
		
		public function UserVO(userName:String, password:String)
		{
			this.userName = userName;
			this.password = password;
		}
	}
}