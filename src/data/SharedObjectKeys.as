package data
{
	public class SharedObjectKeys
	{
		public static const DEVICES:String = "devices";
		public static const USERS:String = "users";
		public static const ADMINS:String = "admins";
		
		public static const LOANS:String = "loans";
		public static const MASTER_PASSWORD:String = "masterPassword";
	}
}