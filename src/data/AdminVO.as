package data
{
	public class AdminVO
	{
		public var userName:String;
		public var password:String;
		
		public function AdminVO(userName:String, password:String)
		{
			this.userName = userName;
			this.password = password;
		}
	}
}